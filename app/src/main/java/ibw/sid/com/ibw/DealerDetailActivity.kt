package ibw.sid.com.ibw

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.activity_dealer_detail.*
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import android.text.Html
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.content_dealer_detail.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson


class DealerDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dealer_detail)
        setSupportActionBar(toolbar)
        toolbar_layout.isTitleEnabled = false
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("")
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))

        if (MySession.company!=null)
        {
            setUI()
        }else
        {
            getMerchant(intent.getStringExtra("mer_id"))
        }
    }


    fun setUI()
    {
        progress_card.visibility = View.GONE
        about_company.text = Html.fromHtml(MySession.company.description)

        phone.text = MySession.company.phone
        email.text = MySession.company.email
        address.text = MySession.company.address
        name.text = MySession.company.person
        offer.text = MySession.company.products.capitalize()
        titleText.text = MySession.company.name

        if(MySession.company.image.isNotEmpty())
        {
            Glide.with(this).load(MySession.company.image).listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    return true
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {

                    val bitmap = (resource as BitmapDrawable).bitmap
                    image.setImageDrawable(resource)

                    Blurry.with(this@DealerDetailActivity).from(bitmap).into(viewBack);

                    Log.d("Detail"," H "+viewBack.height + " W "+viewBack.width)

                    return true

                }
            }).into(image)
        }else
        {
            image.setImageResource(R.drawable.placeholder_big)
            val bitmap = (image.drawable as BitmapDrawable).bitmap
            Blurry.with(this@DealerDetailActivity).from(bitmap).into(viewBack);
        }


        fab.setOnClickListener{

            call(MySession.company.phone)

        }


    }


    fun call(mobile: String)
    {
        if (mobile.isNotBlank())
        {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile))
            startActivity(intent)
        }else
            Toast.makeText(this,"This dealer do not have phone number.", Toast.LENGTH_SHORT).show()

    }

    fun email(email: String)
    {
        if (email.isNotBlank())
        {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Inquiry from IBW")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }else
            Toast.makeText(this,"This dealer do not have email.",Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId==android.R.id.home)
        {
            onBackPressed()

            return  true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun getMerchant(mer_id:String)
    {
        progress_card.visibility  =  View.VISIBLE
        val url = Constants.COMPANIES_API+"/"+mer_id
        Log.e("URL","My ur; "+url)
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    progress_card.visibility  =  View.GONE
                }
                is Result.Success -> {
                    val data = result.get()
                    MySession.company = Gson().fromJson(data,Company::class.java)
                    progress_card.visibility  =  View.GONE

                    setUI()
                }
            }
        }
    }
}
