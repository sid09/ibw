package ibw.sid.com.ibw

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import io.paperdb.Paper
import ibw.sid.com.ibw.*;
import kotlinx.android.synthetic.main.activity_splash.*
import org.json.JSONArray
import org.json.JSONObject

class SplashActivity : AppCompatActivity() {

    var page = 1
    var ads = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setSupportActionBar(toolbar)
        supportActionBar?.hide()
        getAds()
//        startActivity(Intent(this,HomeActivity::class.java))
//        finish()
    }

    fun goNext()
    {
        if (ads)
        {
            startActivity(Intent(this,HomeActivity::class.java))
            finish()
        }else
        {
            val handler = Handler()
            handler.postDelayed({
                //Do something after 100ms
                goNext()
            }, 800)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_splash, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


//    fun buildSearch()
//    {
//        val url = Constants.CAT_API+"?limit=100&page=$page"
//        Log.d("Build Search","Building "+url)
//        url.httpGet().responseString { request, response, result ->
//            //do something with response
//            when (result) {
//                is Result.Failure -> {
//                    val ex = result.getException()
//                    ex.printStackTrace()
//                }
//                is Result.Success -> {
//                    val data = result.get()
//                    val mData = JSONObject(data)
//                    val arr = mData.getJSONArray("rows")
//
//                    for (i in 0 until arr.length())
//                    {
//                        MySession.categories.add(Gson().fromJson(arr.getJSONObject(i).toString(),Category::class.java))
//                    }
//
//
//                    if (page<12)
//                    {
//                        page++
//                        buildSearch()
//                    }else
//                    {
//                        Paper.book().write("search", MySession.categories);
//                        goNext()
//                    }
//
//                }
//            }
//        }
//    }


    fun getAds()
    {
        val url = Constants.ADS_API+"?_sort=sort_order"
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                    ads = true
                }
                is Result.Success -> {
                    val data = result.get()
                    val arr = JSONArray(data)

                    for (i in 0 until arr.length())
                    {
                        MySession.ads.add(Gson().fromJson(arr.getJSONObject(i).toString(),Advertisement::class.java))
                    }
                    Log.d("Advertisements", data);

                    ads = true
                    goNext()
                }
            }
        }
    }
}
