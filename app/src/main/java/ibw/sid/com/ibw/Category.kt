package ibw.sid.com.ibw

data class Category(
        val id: String,
        val name: String,
        val icon: Image,
        val cat_id: String,
        val parent_id: String,
        val keyword: String,
        val enable: Boolean,
        val description: String,
        val createdAt: String,
        val updatedAt: String
)
