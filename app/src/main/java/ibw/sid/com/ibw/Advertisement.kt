package ibw.sid.com.ibw

data class Advertisement(
        val id: String,
        val name: String,
        val image: Image,
        val cat_id: String,
        val company_id: String,
        val expiry: String,
        val sort_order: String,
        val type: String,
        val createdAt: String,
        val updatedAt: String
)