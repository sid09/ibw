package ibw.sid.com.ibw

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_about_us.*
import mehdi.sakout.aboutpage.AboutPage
import mehdi.sakout.aboutpage.Element


class AboutUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setSupportActionBar(toolbar)

        val aboutPage = AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.ibw_logo)
                .addItem(Element().setTitle("1.0"))
                .addGroup("Connect with us")
                .setDescription("Launched in the year 2010 to offer the Indian Business community a platform to promote themselves globally IBW has created a largest B2B marketplace, offering comprehensive business solutions to the Domestic and Global Business Community through its wide array of online services, directory services and facilitation of trade promotional events. Our portal is an ideal forum for buyers and sellers across the globe to interact and conduct business smoothly and effectively.\n" +
                        "\n" +
                        "IBW is maintained and promoted by Industrial Business World. Today we have around 500,000 companies and dealers in database and we are growing fastly.")
                .addEmail("industrialbusinessworld@gmail.com")
                .addWebsite("ibw.co.in")
                .addPlayStore(packageName)
                .create()

        setContentView(aboutPage)
    }

}
