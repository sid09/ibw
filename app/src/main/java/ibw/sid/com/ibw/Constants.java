package ibw.sid.com.ibw;

public class Constants {

    public static String BASE_URL = "https://ibw.net.in:8443/";
    public static String CAT_API = BASE_URL+"categories";
    public static String COMPANIES_API = BASE_URL+"companies";
    public static String ADS_API = BASE_URL+"advertisements";

}
