package ibw.sid.com.ibw


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_search.*
import org.json.JSONArray
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : androidx.fragment.app.Fragment() {

    var categories = ArrayList<Category>()
    lateinit var adapter:CategoriesAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val mFirebaseAnalytics = context?.let { FirebaseAnalytics.getInstance(it) }
        adapter = CategoriesAdapter(context,categories, CallBack {

            Log.d("Cat", "CLICK POST " + categories.get(it).parent_id)

            if (categories.get(it).parent_id == "0")
            {
                val bundle = Bundle()
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, categories.get(it).cat_id)
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, categories.get(it).name)
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "parent_category")
                mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
                startActivity(Intent(context,CategoryActivity::class.java).putExtra("cat_id",categories.get(it).cat_id).putExtra("name",categories.get(it).name))


            }else
            {
                val bundle = Bundle()
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, categories.get(it).cat_id)
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, categories.get(it).name)
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "child_category")
                mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
                startActivity(Intent(context,DealersListActivity::class.java).putExtra("id",categories.get(it).id).putExtra("cat_id",categories.get(it).cat_id).putExtra("name",categories.get(it).name))

            }

        })
        recyclerView.layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 3)
        recyclerView.adapter = adapter

        search_text.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {

                if (editable.toString().length>2)
                searchCategory(search_text.text.toString().toLowerCase())
            }
        })

    }


    fun searchCategory(key: String)
    {
        val url = Constants.CAT_API+"?&_sort=name&_limit=1200&name_contains="+key
        Log.e("Search", "Searching for "+ url)
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    ex.printStackTrace()
                }
                is Result.Success -> {
                    val data = result.get()
                    val arr = JSONArray(data)
                    categories.clear()

                    for (i in 0 until arr.length())
                    {
                        val cat = Gson().fromJson(arr.getJSONObject(i).toString(),Category::class.java);
                        if(cat.enable)
                            categories.add(Gson().fromJson(arr.getJSONObject(i).toString(),Category::class.java))
                    }

                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

}
