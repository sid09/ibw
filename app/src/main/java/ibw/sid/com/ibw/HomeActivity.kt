package ibw.sid.com.ibw

import android.content.Intent
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import kotlinx.android.synthetic.main.activity_home.*
import android.view.MenuInflater
import android.view.MenuItem
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPut
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import io.paperdb.Paper
import org.json.JSONObject


class HomeActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, HomeFragment())
                }
                supportActionBar?.title = "  Home"
                supportActionBar?.show()


                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorites -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, FavouritesFragment())
                }
                supportActionBar?.title = "  Favorites"
                supportActionBar?.show()

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {

                supportFragmentManager.inTransaction {
                    replace(R.id.frame, SearchFragment())
                }
                supportActionBar?.title = "  Search"
                supportActionBar?.hide()

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        toolbar.setLogo(R.mipmap.ic_launcher)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        supportFragmentManager.inTransaction {
            replace(R.id.frame, HomeFragment())
        }
        supportActionBar?.title = "  Home"

    }

    inline fun androidx.fragment.app.FragmentManager.inTransaction(func: androidx.fragment.app.FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.commit()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_splash, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId==R.id.action_info)
        {
            startActivity(Intent(this,AboutUsActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }

//    fun startICons()
//    {
//        for (category in MySession.categories)
//        {
//            changeIcon(category)
//
//        }
//    }
//
//    fun changeIcon(cat: Category)
//    {
//        "http://ibw.net.in:8080/categories/"+cat.id.httpPut( listOf("cat_id" to cat.cat_id, "icon" to cat.icon, "parent_id" to cat.parent_id, "name" to cat.name, "keyword" to cat.keyword)).responseString { request, response, result ->
//            //do something with response
//            when (result) {
//                is Result.Failure -> {
//                    val ex = result.getException()
//                    ex.printStackTrace()
//                }
//                is Result.Success -> {
//                    val data = result.get()
//                    val mData = JSONObject(data)
//                }
//            }
//        }
//    }
}
