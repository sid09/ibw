package ibw.sid.com.ibw

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class SlideAdapter(val urls:ArrayList<Advertisement>,val manager: androidx.fragment.app.FragmentManager)  : androidx.fragment.app.FragmentStatePagerAdapter(manager) {


    override fun getItem(position: Int): androidx.fragment.app.Fragment {

        val bundle = Bundle()
        bundle.putString("url",Constants.BASE_URL + urls.get(position).image.url)
        val fragment = ImageFragment()
        fragment.arguments = bundle

        return fragment
    }


    override fun getCount(): Int {
       return urls.size
    }
}