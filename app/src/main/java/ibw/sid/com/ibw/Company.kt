package ibw.sid.com.ibw

data class Company(
        val id: String,
        val email: String,
        val name: String,
        val image: String,
        val person: String,
        val products: String,
        val website: String,
        val phone: String,
        val address: String,
        val city: String,
        val description: String,
        val category: Category,
        val createdAt: String,
        val updatedAt: String,
        val isPaid: Boolean,
        val thumbnail: String,
        val enable: Boolean
)