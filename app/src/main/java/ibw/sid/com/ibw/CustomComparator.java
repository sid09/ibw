package ibw.sid.com.ibw;

import java.util.Comparator;

public class CustomComparator implements Comparator<Company> {

    @Override
    public int compare(Company o1, Company o2) {

        if(!o2.isPaid())
        {
            return (o2.getImage().length() - (o1.getImage().length()));
        }else
            return 0;
    }
}
