package ibw.sid.com.ibw

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.category_layout.view.*
import java.util.ArrayList

class CategoriesAdapter (val context: Context?, val categories: ArrayList<Category>, val callBack: CallBack) : androidx.recyclerview.widget.RecyclerView.Adapter<CategoriesAdapter.CCAdapterViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CCAdapterViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.category_layout, parent, false)
        return CCAdapterViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return categories.size
    }


    override fun onBindViewHolder(holder: CCAdapterViewHolder, position: Int) {

        holder.bind(position)
    }



    inner class CCAdapterViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {


        fun bind(pos:Int) = with(itemView) {

            title.text = categories.get(pos).name
            Glide.with(context).load(Constants.BASE_URL + categories.get(pos).icon.url).into(icon)
            cardView.setOnClickListener {
                Log.d("Cat Click", categories.get(pos).name);
                callBack.onClick(pos)

            }


        }


    }


}