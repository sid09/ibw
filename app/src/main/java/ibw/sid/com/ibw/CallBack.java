package ibw.sid.com.ibw;

public interface CallBack {

    void onClick(int position);
}
