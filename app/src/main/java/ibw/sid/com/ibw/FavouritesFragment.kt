package ibw.sid.com.ibw


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_favourites.*


class FavouritesFragment : androidx.fragment.app.Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        if (Paper.book().contains("favs"))
        {
            MySession.fav = Paper.book().read("favs")
            recyclerView.adapter = CompaniesAdapter(context,MySession.fav, CallBack {

                startActivity(Intent(context,DealerDetailActivity::class.java))

            })
        }


    }

}
