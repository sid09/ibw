package ibw.sid.com.ibw

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_dealers_list.*
import org.json.JSONObject
import androidx.recyclerview.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.github.kittinunf.fuel.httpGet
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.content_dealers_list.*
import org.json.JSONArray
import java.util.*


class DealersListActivity : AppCompatActivity() {

    val companies = ArrayList<Company>()
    var page = 1
    var noData = false
    var catId = ""
    var id = ""
    lateinit var adapter:CompaniesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dealers_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        supportActionBar?.setTitle(intent.getStringExtra("name"))
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        catId = intent.getStringExtra("cat_id")
        id = intent.getStringExtra("id")

        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        adapter = CompaniesAdapter(this,companies, CallBack {

            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, companies.get(it).id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, companies.get(it).name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "company")
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)

            MySession.company = companies.get(it)
            startActivity(Intent(this,DealerDetailActivity::class.java))

        })

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(mPage: Int, totalItemsCount: Int, view: androidx.recyclerview.widget.RecyclerView?) {

//                if (!noData && page<300)
//                {
//                    page++
//                    getDealers();
//
//                }

            }

        })
        recyclerView.adapter = adapter

        val ad = MySession.popUpExists(catId)
        if (ad!=null)
        {
            //show ad
            Log.d("Popup","URl "+ Constants.BASE_URL + ad.image)
            Glide.with(this).load(Constants.BASE_URL + ad.image.url).into(popup)
            popupAd.visibility = View.VISIBLE

            popup.setOnClickListener {
                if (ad.cat_id != "-1"){
                    MySession.company = null
                    Log.d("Company","ID company "+ad.company_id)
                    startActivity(Intent(this,DealerDetailActivity::class.java).putExtra("mer_id",ad.company_id))
                } else
                {
                    call("+918968299168")
                }

            }
        }

        close.setOnClickListener {

            popupAd.visibility = View.GONE

        }

        getDealers()
    }

    override fun onBackPressed() {
        if (popupAd.visibility==View.VISIBLE)
            popupAd.visibility = View.GONE
        else
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId==android.R.id.home)
        {
            onBackPressed()

            return  true
        }

        return super.onOptionsItemSelected(item)
    }


    fun call(mobile: String)
    {
        if (mobile.isNotBlank())
        {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile))
            startActivity(intent)
        }else
            Toast.makeText(this,"This dealer do not have phone number.", Toast.LENGTH_SHORT).show()

    }




    fun  getDealers()
    {

        progress.visibility = View.VISIBLE
        val url = Constants.COMPANIES_API+"?_sort=sort_order&category="+id+"&_limit=2000"
        Log.d("URL","Call to url "+url)
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                    progress.visibility = View.GONE
                    if (companies.size==0)
                        empty.visibility = View.VISIBLE
                }
                is Result.Success -> {
                    progress.visibility = View.GONE
                    val data = result.get()
                    val arr = JSONArray(data)

                    if (arr.length()==0)
                        noData = true

                    for (i in 0 until arr.length())
                    {
                        val company = Gson().fromJson(arr.getJSONObject(i).toString(),Company::class.java)
                        if (company.enable)
                        companies.add(company)
                    }

                    companies.reverse()
                 //  Collections.sort(companies,CustomComparator())

                    if (companies.size==0)
                        empty.visibility = View.VISIBLE

                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

}
