package ibw.sid.com.ibw

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import com.google.firebase.analytics.FirebaseAnalytics
import org.json.JSONArray


class HomeFragment : androidx.fragment.app.Fragment() {

    val categories = ArrayList<Category>()
    lateinit var adapter:CategoriesAdapter
    val banners = ArrayList<Advertisement>()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        addBanners()
        slider.adapter = SlideAdapter(banners,childFragmentManager)
        indicator.setViewPager(slider)
        slider.setOnItemClickListener {

            if (banners[it].company_id!="0")
            {
                MySession.company = null
                Log.d("Company","ID company "+banners[it].company_id)
                startActivity(Intent(context,DealerDetailActivity::class.java).putExtra("mer_id",banners[it].company_id))
            }
            else
            {
                call("+919592948102")
            }
        }
        val mFirebaseAnalytics = context?.let { FirebaseAnalytics.getInstance(it) }

        adapter = CategoriesAdapter(context,categories, CallBack {

            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, categories.get(it).cat_id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, categories.get(it).name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "parent_category")
            mFirebaseAnalytics?.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
            startActivity(Intent(context,CategoryActivity::class.java).putExtra("cat_id",categories.get(it).cat_id).putExtra("name",categories.get(it).name))


        })
        recyclerView.layoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 3)
        recyclerView.adapter = adapter

        getCategories()

    }

    fun addBanners()
    {
        banners.clear()
        for (Ad in MySession.ads)
        {
            if (Ad.type.toLowerCase() == "banner")
            {
                banners.add(Ad)
            }
        }
    }

    fun call(mobile: String)
    {
        if (mobile.isNotBlank())
        {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile))
            startActivity(intent)
        }else
            Toast.makeText(context,"This dealer do not have phone number.", Toast.LENGTH_SHORT).show()

    }


    fun getCategories()
    {
        Log.e("CAT","Constants.CAT_API "+Constants.CAT_API)
        val url = Constants.CAT_API+"?_sort=name&_limit=100&parent_id=0"
        url.httpGet().responseString { request, response, result ->
        //do something with response
        when (result) {
            is Result.Failure -> {
                val ex = result.getException()
            }
            is Result.Success -> {
                val data = result.get()
                val arr = JSONArray(data)
                categories.clear()

                for (i in 0 until arr.length())
                {
                    val cat = Gson().fromJson(arr.getJSONObject(i).toString(),Category::class.java);
                    if(cat.enable)
                        categories.add(cat)
                }

                adapter.notifyDataSetChanged()
                }
            }
        }

    }


}
