package ibw.sid.com.ibw

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.company_layout.view.*
import java.util.ArrayList
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.ImageView
import io.paperdb.Paper


class CompaniesAdapter (val context: Context?, val categories: ArrayList<Company>, val callBack: CallBack) : androidx.recyclerview.widget.RecyclerView.Adapter<CompaniesAdapter.CCAdapterViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CCAdapterViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.company_layout, parent, false)
        return CCAdapterViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return categories.size
    }


    override fun onBindViewHolder(holder: CCAdapterViewHolder, position: Int) {
        holder.bind(position)
    }



    inner class CCAdapterViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {


        fun bind(pos:Int) = with(itemView) {

           Log.d("Image", categories.get(pos).name + " Image : " + categories.get(pos).image + " Thumb " + categories.get(pos).thumbnail)
            company_title.text = categories.get(pos).name.capitalize()

            if (categories.get(pos).thumbnail.startsWith("http"))
                Glide.with(context).load(categories.get(pos).thumbnail).into(image)
            else if (categories.get(pos).image.isNotBlank())
            Glide.with(context).load(categories.get(pos).image).into(image)
            else
            {
                Glide.with(context).load(R.drawable.no_image).into(image)
                image.scaleType = ImageView.ScaleType.CENTER_CROP
            }

            Log.d("IMage","H : "+image.height+" "+image.width)

            var cityVal = categories.get(pos).city.capitalize()

            if (categories.get(pos).city.contains(","))
                if (categories.get(pos).city.split(",")[0] == categories.get(pos).city.split(",")[1])
                    cityVal = categories.get(pos).city.split(",")[0].capitalize()


            city.text = cityVal
            products.text = categories.get(pos).products.capitalize()

            if (MySession.fav.contains(categories.get(pos)))
                fav_icon.setImageResource(R.drawable.ic_favorite_red_24dp)
            else
                fav_icon.setImageResource(R.drawable.ic_favorite_border_red_24dp)



            cardView.setOnClickListener {

                callBack.onClick(pos)

            }

            fav.setOnClickListener{

                if (!MySession.fav.contains(categories.get(pos)))
                {
                    MySession.fav.add(categories.get(pos))
                    Paper.book().write("favs",MySession.fav)
                    fav_icon.setImageResource(R.drawable.ic_favorite_red_24dp)
                    Toast.makeText(context,"Item added to your favorites",Toast.LENGTH_SHORT).show()
                }else
                {
                    MySession.fav.remove(categories.get(pos))
                    Paper.book().write("favs",MySession.fav)
                    fav_icon.setImageResource(R.drawable.ic_favorite_border_red_24dp)
                    Toast.makeText(context,"Item removed from your favorites",Toast.LENGTH_SHORT).show()
                }


            }
            call.setOnClickListener{call(categories.get(pos).phone)}

            email.setOnClickListener {email(categories.get(pos).email)}

        }


    }


    fun email(email: String)
    {
        if (email.isNotBlank())
        {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", email, null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Inquiry from IBW")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            context?.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }else
            Toast.makeText(context,"This dealer do not have email.",Toast.LENGTH_SHORT).show()
    }

    fun call(mobile: String)
    {
        if (mobile.isNotBlank())
        {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobile))
            context?.startActivity(intent)
        }else
        Toast.makeText(context,"This dealer do not have phone number.",Toast.LENGTH_SHORT).show()

    }


}