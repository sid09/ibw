package ibw.sid.com.ibw

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import android.view.MenuItem
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson

import kotlinx.android.synthetic.main.activity_category.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONArray
import org.json.JSONObject

class CategoryActivity : AppCompatActivity() {


    val categories = ArrayList<Category>()
    lateinit var adapter:CategoriesAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        supportActionBar?.title = intent.getStringExtra("name")
        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        adapter = CategoriesAdapter(this,categories, CallBack {
            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, categories.get(it).cat_id)
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, categories.get(it).name)
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "child_category")
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
            startActivity(Intent(this,DealersListActivity::class.java).putExtra("id",categories.get(it).id).putExtra("cat_id",categories.get(it).cat_id).putExtra("name",categories.get(it).name))

        })
        recyclerView.layoutManager = androidx.recyclerview.widget.GridLayoutManager(this, 3)
        recyclerView.adapter = adapter

        getCategories(intent.getStringExtra("cat_id"))
    }

    fun getCategories(id:String)
    {
        val url = Constants.CAT_API+"?&_sort=name&_limit=1000&parent_id="+id
        Log.e("Category GOT", "RCVD IT" + url)
        url.httpGet().responseString { request, response, result ->
            //do something with response
            when (result) {
                is Result.Failure -> {
                    val ex = result.getException()
                }
                is Result.Success -> {
                    val data = result.get()
                    val arr = JSONArray(data)
                    categories.clear()

                    for (i in 0 until arr.length())
                    {
                        val cat = Gson().fromJson(arr.getJSONObject(i).toString(),Category::class.java);
                        if(cat.enable)
                        categories.add(cat)
                    }

                    adapter.notifyDataSetChanged()
                }
            }
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId==android.R.id.home)
        {
            onBackPressed()

            return  true
        }

        return super.onOptionsItemSelected(item)
    }

}
